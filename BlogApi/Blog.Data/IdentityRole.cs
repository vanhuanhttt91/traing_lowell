﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class IdentityRole
    {
        public string Id { get; set; }
        public string NormalizedName { get; set; }
        public string Name { get; set; }
    }
}

