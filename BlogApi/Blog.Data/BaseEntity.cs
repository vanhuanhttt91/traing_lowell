﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class BaseEntity
    {
        public string Label { get; set; }
        public string Node { get; set; }
        public int OrderIndex { get; set; }
    }
}
