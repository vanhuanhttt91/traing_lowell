﻿using Blog.Data;
using Blog.Repo;
using Neo4j.Driver.V1;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.service
{
    public class CategoriesService : ICategoriesService
    {
        private IRepository<Categories> categoriesReposity;
        public CategoriesService(IRepository<Categories> categoriesReposity)
        {
            this.categoriesReposity = categoriesReposity;
        }
        /// <summary>
        /// create categories
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public async Task<DataResult> CreateSync(Categories categories)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "CREATE (c:Categories) " +
                                   "SET  c.Id = $Id, c.Name = $Name, c.Description =$Description, " +
                                   "     c.CreateDate = $CreateDate, c.UpdateDate = $UpdateDate " +
                                   "     RETURN c.Id, c.Name, c.Description, c.CreateDate, c.UpdateDate";
                var obj = new
                {
                    Id = id,
                    Name = categories.Name,
                    Description = categories.Description,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };
                await categoriesReposity.InsertAsync(statement, obj);
                return new DataResult { IsError = false, Message = "Add new categories success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        /// <summary>
        /// delete categories
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<DataResult> DeleteSync(string id)
        {
            try
            {
                string statement = " match(n:Categories {Id: $Id}) delete n";
                var obj = new
                {
                    Id = id
                };
                await categoriesReposity.DeleteAsync(statement, obj);
                return new DataResult { IsError = false, Message = "Remove Success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        /// <summary>
        /// edit categories
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public async Task<DataResult> EditSync(Categories categories)
        {
            try
            {
                string statement = " MATCH (n: Categories { Id: $Id }) " +
                                   " SET n.Name = $Name, n.Description = $Description,  n.UpdateDate = $UpdateDate" +
                                   " RETURN n.Name;";
                var obj = new
                {
                    Id = categories.Id,
                    Name = categories.Name,
                    Description = categories.Description,
                    UpdateDate = DateTime.Now
                };
                await categoriesReposity.UpdateAsync(statement, obj);
                return new DataResult { IsError = false, Message = "Update Success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        /// <summary>
        /// get all data
        /// </summary>
        /// <returns></returns>
        public async Task<DataResult> GetDataAsync()
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (n:Categories) RETURN n.Id as Id, n.Name as Name, n.Description as Description, n.CreateDate as CreateDate, n.UpdateDate as UpdateDate";
                var cursor = await categoriesReposity.GetAllAsync(statement);
                var data = await GetCategoriesListAsync(cursor);
                return new DataResult { IsError = false, Message = "Lấy categories thành công!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }

        public async Task<DataResult> GetDataByPostIdAsync(string postId)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (n:Post)-[r:dependent]->(c:Categories {Id: '"+ postId + "'}) RETURN n.Id as Id, n.Name as Name, n.Description as Description, n.CreateDate as CreateDate, n.UpdateDate as UpdateDate";
                var cursor = await categoriesReposity.GetAllAsync(statement);
                var data = await GetCategoriesListAsync(cursor);
                return new DataResult { IsError = false, Message = "get categories success!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }

        public async Task<DataResult> GetDetails(string id)
        {
            try
            {
                string statement = "MATCH (n:Categories {Id: "+id+"}) RETURN n.Id as Id, n.Name as Name, n.Description as Description, n.CreateDate as CreateDate, n.UpdateDate as UpdateDate";
                var cursor = await categoriesReposity.GetAllAsync(statement);
                var data = await GetCategoriesListAsync(cursor);
                return new DataResult { IsError = false, Message = "Lấy categories thành công!", Item = data };
            }
            catch (Exception ex)
            {
              
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cursor"></param>
        /// <returns></returns>
        private async Task<List<Categories>> GetCategoriesListAsync(IStatementResultCursor cursor)
        {
            List<Categories> lstCategories = new List<Categories>();
            var i = 0;
            while (await cursor.FetchAsync())
            {
                i++;
                lstCategories.Add(new Categories
                {
                    OrderIndex = i,
                    Id = cursor.Current.Values["Id"].ToString(),
                    Name = cursor.Current.Values["Name"].ToString(),
                    Description = cursor.Current.Values["Description"].ToString(),
                    CreateDate = Convert.ToDateTime(cursor.Current.Values["CreateDate"]),
                    UpdateDate = Convert.ToDateTime(cursor.Current.Values["UpdateDate"])
                });
            }
            return lstCategories;
        }
    }
}
