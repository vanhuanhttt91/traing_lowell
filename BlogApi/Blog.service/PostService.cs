﻿using Blog.Data;
using Blog.Repo;
using Neo4j.Driver.V1;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.service
{
    public class PostService : IPostService
    {
        private IRepository<Post> postReposity;
        public PostService(IRepository<Post> postReposity)
        {
            this.postReposity = postReposity;
        }
        public async Task<DataResult> CreateSync(Post post)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "CREATE (c:Post) " +
                                   "SET  c.Id = $Id, c.Slug = $Slug, c.ShortDescription =$ShortDescription, c.ThumbnailImage =$ThumbnailImage, " +
                                   "     c.Content =$Content, c.CreateDate = $CreateDate, c.UpdateDate = $UpdateDate " +
                                   "     RETURN c.Id as Id, c.Slug as Slug, c.ShortDescription as ShortDescription, " +
                                   "     c.ThumbnailImage as ThumbnailImage, c.Content as Content, c.CreateDate as CreateDate, c.UpdateDate as UpdateDate";
                var obj = new
                {
                    Id = id,
                    Slug = post.Slug,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImage = post.ThumbnailImage,
                    Content = post.Content,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };
                var cursor = await postReposity.InsertAsyncByCursor(statement, obj);
                var dataPost = await GetPostListAsync(cursor);
                post.Id = dataPost[0].Id;
                var relation = await CreateRelationSync(post);
                var data = await GetDataSync();// get 
                return new DataResult { IsError = false, Message = "Add new post success!", Item = data.Item };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> DeleteSync(string id)
        {
            try
            {
                string statement = " MATCH (p:Post { Id: $Id }) DETACH DELETE p ";
                var obj = new
                {
                    Id = id
                };
                await postReposity.DeleteAsync(statement, obj);
                var data = await GetDataSync();// get 
                return new DataResult { IsError = false, Message = "Remove Success!", Item = data.Item };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> EditSync(Post post)
        {
            var session = postReposity.GetDriver().Session();
            var tx = await session.BeginTransactionAsync();
            try
            {
                string updateStatement = " MATCH (c: Post { Id: $Id }) " +
                                   " SET c.Id = $Id, c.Slug = $Slug, c.ShortDescription =$ShortDescription, c.ThumbnailImage =$ThumbnailImage, " +
                                   " c.Content =$Content, c.UpdateDate = $UpdateDate " +
                                   " RETURN c.Id;";
                var objUpdate = new
                {
                    Id = post.Id,
                    Slug = post.Slug,
                    ShortDescription = post.ShortDescription,
                    ThumbnailImage = post.ThumbnailImage,
                    Content = post.Content,
                    UpdateDate = DateTime.Now
                };
                string deleteStatement = " MATCH (p:Post {Id:$pId})-[r:dependent]->(c:Categories) delete r";
                var objDel = new
                {
                    pId = post.Id
                };
                string createStatement = "match (c:Categories), (p: Post) " +
                               "where c.Id = $cId and p.Id = $pId " +
                               "create (p)-[:dependent]->(c) " +
                               "return p, c ";
                var objCreate = new
                {
                    cId = post.CategoriesId,
                    pId = post.Id
                };
                await postReposity.UpdateTransactionAsync(tx, updateStatement, objUpdate);// update post
                await postReposity.DeleteTransactionAsync(tx, deleteStatement, objDel);// delete relationsip
                await postReposity.CreateTransactionAsync(tx, createStatement, objCreate);// create new relationship
                // Commit the transaction
                await tx.CommitAsync();
                var data = await GetDataSync();// get data
                return new DataResult { IsError = false, Message = "Update Success!", Item = data.Item };
            }
            catch (Exception ex)
            {
                await tx.RollbackAsync();
                return new DataResult { IsError = true, Message = ex.Message };
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        public async Task<DataResult> GetDataSync()
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (c:Post)-[r:dependent]->(p:Categories)  RETURN  c.Id as Id, c.Slug as Slug, c.ShortDescription as ShortDescription, " +
                                   " c.ThumbnailImage as ThumbnailImage, c.Content as Content, c.CreateDate as CreateDate, c.UpdateDate as UpdateDate, p.Id as CategoriesId" +
                                   " ORDER BY CreateDate DESC";
                var cursor = await postReposity.GetAllAsync(statement);
                var data = await GetPostListAsync(cursor);
                return new DataResult { IsError = false, Message = "get post success!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> GetDataByPostIdAsync(string postId)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (c:Post)-[r:dependent]->(p:Categories {Name: '" + postId + "'}) RETURN  c.Id as Id, c.Slug as Slug, c.ShortDescription as ShortDescription, " +
                                   " c.ThumbnailImage as ThumbnailImage, c.Content as Content, c.CreateDate as CreateDate, c.UpdateDate as UpdateDate, p.Id as CategoriesId" +
                                   " ORDER BY CreateDate DESC";
                var cursor = await postReposity.GetAllAsync(statement);
                var data = await GetPostListAsync(cursor);
                return new DataResult { IsError = false, Message = "get categories success!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> GetDetailsSync(string id)
        {
            try
            {
                string statement = "MATCH (c:Post {Slug: '" + id + "'})-[r:dependent]->(p:Categories)  RETURN  c.Id as Id, c.Slug as Slug, c.ShortDescription as ShortDescription, " +
                                   " c.ThumbnailImage as ThumbnailImage, c.Content as Content, c.CreateDate as CreateDate, c.UpdateDate as UpdateDate, p.Id as CategoriesId " +
                                   " ORDER BY CreateDate DESC";
                var cursor = await postReposity.GetAllAsync(statement);
                var data = await GetPostListAsync(cursor);
                return new DataResult { IsError = false, Message = "get post success!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> CreateRelationSync(Post post)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "match (c:Categories), (p: Post) " +
                                   "where c.Id = $cId and p.Id = $pId " +
                                   "create (p)-[:dependent]->(c) " +
                                   "return p, c ";
                var obj = new
                {
                    cId = post.CategoriesId,
                    pId = post.Id
                };
                await postReposity.CreateRelationAsync(statement, obj);
                return new DataResult { IsError = false, Message = "Add Relationship success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        public async Task<DataResult> DeleteRelationShipSync(Post post)
        {
            try
            {
                string statement = " MATCH (p:Post {Id:$pId})-[r:dependent]->(c:Categories) delete r";
                var obj = new
                {
                    pId = post.Id
                };
                await postReposity.DeleteRelationAsync(statement, obj);
                return new DataResult { IsError = false, Message = "Remove Success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
        private async Task<List<Post>> GetPostListAsync(IStatementResultCursor cursor)
        {
            List<Post> lstCategories = new List<Post>();
            var i = 0;
            while (await cursor.FetchAsync())
            {
                i++;
                lstCategories.Add(new Post
                { //c.Id, c.Slug, c.ShortDescription, c.ThumbnailImage, c.Content, c.CreateDate, c.UpdateDate 
                    OrderIndex = i,
                    Id = cursor.Current.Values["Id"].ToString(),
                    Slug = cursor.Current.Values["Slug"].ToString(),
                    ShortDescription = cursor.Current.Values["ShortDescription"].ToString(),
                    ThumbnailImage = cursor.Current.Values["ThumbnailImage"].ToString(),
                    Content = cursor.Current.Values["Content"].ToString(),
                    UpdateDate = Convert.ToDateTime(cursor.Current.Values["UpdateDate"]),
                    CreateDate = Convert.ToDateTime(cursor.Current.Values["CreateDate"]),
                    CategoriesId = cursor.Current.Keys.Count == 8 ? cursor.Current[7].ToString() : null,
                });
            }
            return lstCategories;
        }

        public async Task<DataResult> GetLastPostaSync()
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (c:Post) RETURN  c.Id as Id, c.Slug as Slug, c.ShortDescription as ShortDescription, " +
                                   " c.ThumbnailImage as ThumbnailImage, c.Content as Content, c.CreateDate as CreateDate, c.UpdateDate as UpdateDate" +
                                   " ORDER BY CreateDate DESC limit 5";
                var cursor = await postReposity.GetAllAsync(statement);
                var data = await GetPostListAsync(cursor);
                return new DataResult { IsError = false, Message = "get post success!", Item = data };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }
    }
}
