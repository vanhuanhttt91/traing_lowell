﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Neo4j.Driver.V1;
using Blog.service;
using Blog.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace BlogApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : Controller
    {

        private readonly ICategoriesService categoriesService;
        public CategoriesController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        // GET api/values/5
        [HttpGet()]
        public async Task<ActionResult<DataResult>> Get()
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);
            }
            var result = await categoriesService.GetDataAsync();
            return Ok(result);
        }
        [HttpGet("getdetail")]
        public async Task<ActionResult<DataResult>> GetDetail(string id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest(ModelState);
            }
            var result = await categoriesService.GetDetails(id);
            return Ok(result);
        }
        [HttpGet("getbypostid/{id}")]
        public async Task<ActionResult<DataResult>> GetDetailByPostId(string id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest(ModelState);
            }
            var result = await categoriesService.GetDataByPostIdAsync(id);
            return Ok(result);
        }
        [HttpPost("add"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Create([FromBody]Categories categories)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await categoriesService.CreateSync(categories);
            var data = await categoriesService.GetDataAsync();
            if (!data.IsError) data.Message = result.Message;
            return Ok(data);
        }

        [HttpGet("delete/{id}"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Delete(string id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest(ModelState);
            }
            var result = await categoriesService.DeleteSync(id);
            var data = await categoriesService.GetDataAsync();
            if (!data.IsError) data.Message = result.Message;
            return Ok(data);
        }

        [HttpPost("edit"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Edit([FromBody]Categories categories)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await categoriesService.EditSync(categories);
            var data = await categoriesService.GetDataAsync();
            if (!data.IsError) data.Message = result.Message;
            return Ok(data);
        }
    }
}
