﻿using AspNetCore.Identity.Neo4j;
using Blog.Api.ViewModal;
using Blog.Data;
using Blog.service;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace Blog.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<Neo4jIdentityRole> _roleManager;
        private readonly IAccountService _accountService;
        public AccountController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<Neo4jIdentityRole> roleManager,
            IAccountService accountService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _accountService = accountService;


        }
        public async Task Seed()
        {
            /* Create Roles */
            List<Neo4jIdentityRole> allRoles = new List<Neo4jIdentityRole>();
            allRoles.Add(new Neo4jIdentityRole { Id = "1", Name = "Manager", NormalizedName= "Manager" });
            foreach (Neo4jIdentityRole role in allRoles)
            {
                if (!await _roleManager.RoleExistsAsync(role.ToString()))
                {
                    await _roleManager.CreateAsync(new Neo4jIdentityRole
                    {
                        Name = role.ToString(),
                        NormalizedName = role.ToString().ToUpper()
                    });
                }
            }
        }
        [HttpPost]
        public async Task<ActionResult<DataResult>> Create([FromBody] User model)
        {
            var result = new DataResult();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                    var addUser = await _userManager.CreateAsync(user, model.Password);
                    var checkUser = await _userManager.CheckPasswordAsync(user, model.Password);
                    var findUser = await _userManager.FindByEmailAsync(user.Email);
                    await Seed();
                    
                    //var isRoke = await _userManager.IsInRoleAsync(findUser, "Manager");
                    var GET = await _userManager.GetUsersInRoleAsync("MANAGER");

                    var addRole = await _accountService.AddToRoleAsync(user.Email);
                    var lstErrorMessage = new StringBuilder();
                    if (checkUser)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);

                    }
                    else
                    {
                        result.IsError = true;
                        result.Message = lstErrorMessage.ToString();
                    }
                }
                else
                {
                    result = new DataResult { IsError = true, Message = ModelState.Values.ToString() };
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                result = new DataResult { IsError = true, Message = ex.Message };
                return Ok(result);
            }
        }

        // GET api/values
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody]User user)
        {
            if (ModelState.IsValid)
            {

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var findUser = await _userManager.FindByEmailAsync(user.Email);
                var checkUser = await _userManager.CheckPasswordAsync(findUser, user.Password);
                var result = await _signInManager.PasswordSignInAsync(user.Email, user.Password, user.RememberMe, lockoutOnFailure: true);
                var getRole = await _userManager.GetRolesAsync(findUser);
                if (result.Succeeded)
                {
                    var role = await _accountService.GetRoleByEmai(user.Email);
                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                    var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                    var tokeOptions = new JwtSecurityToken(
                        issuer: "http://localhost:44383",
                        audience: "http://localhost:44383",
                        claims: new List<Claim>(),
                        expires: DateTime.Now.AddDays(1),
                        signingCredentials: signinCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                    return Ok(new { Token = tokenString, role = role, isError= false });
                }
                else
                {
                    return Ok(new { isError=true, message="login fail!" });
                }
            }
            else
            {
                return BadRequest("Invalid client request");

            }
        }
    }

}