﻿using System;
using System.Collections.Generic;
using System.Text;
using Neo4j.Driver.V1;
namespace Blog.Repo
{
    public interface IApplicationConnect
    {
        IDriver GetDriver();
        void Dispose();
    }

}
