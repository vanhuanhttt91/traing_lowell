﻿using Neo4j.Driver.V1;
using System;
using System.Threading.Tasks;

namespace Blog.Repo
{
    public class Repository<T> : IRepository<T>
    {
        private IApplicationConnect _context;

        public IDriver GetDriver()
        {
            return this._context.GetDriver();
        }
        public Repository(IApplicationConnect context)
        {
            this._context = context;
        }

        public T Get(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IStatementResultCursor> GetAllAsync(string statement)
        {
            try
            {
                IStatementResultCursor reader;
                using (var session = _context.GetDriver().Session())
                {
                    reader = await session
                       .RunAsync(statement);

                }
                return reader;
            }
            catch (Exception ex)
            {
                
                throw new NotImplementedException(ex.Message);
            }
            finally
            {
               // Dispose();
            }
        }

        public async Task InsertAsync(string statement, object parameter)
        {
            try
            {
                 
                using (var session = _context.GetDriver().Session())
                {
                   var reader = await session
                       .RunAsync(statement, parameter);

                }
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
              //  Dispose();
            }
        }

        public async Task DeleteAsync(string statement, object parameter)
        {
            try
            {
                using (var session = _context.GetDriver().Session())
                {
                  var  reader = await session
                       .RunAsync(statement, parameter);
                }
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
               // Dispose();
            }
        }
 
        Task IRepository<T>.SaveChanges()
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(string statement, object parameter)
        {
            try
            {

                using (var session = _context.GetDriver().Session())
                {
                    var reader = await session
                        .RunAsync(statement, parameter);

                }
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
               // Dispose();
            }
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task CreateRelationAsync(string statement, object parameter)
        {
            try
            {

                using (var session = _context.GetDriver().Session())
                {
                    var reader = await session
                        .RunAsync(statement, parameter);

                }
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
               // Dispose();
            }
        }

        /// <summary>
        /// insert node return IStatementResultCursor
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<IStatementResultCursor> InsertAsyncByCursor(string statement, object parameter )
        {
            try
            {
                using (var session = _context.GetDriver().Session())
                {
                    var cursor = await session
                        .RunAsync(statement, parameter);
                    return cursor;
                }
                
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
               // Dispose();
            }
        }

        public async Task DeleteRelationAsync(string statement, object parameter)
        {
            try
            {

                using (var session = _context.GetDriver().Session())
                {
                    var reader = await session
                        .RunAsync(statement, parameter);

                }
            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
                // Dispose();
            }
        }

        public async Task<IStatementResultCursor> GetRoleByUser(string statement, object parameter)
        {
            try
            {
                using (var session = _context.GetDriver().Session())
                {
                    var reader = await session
                        .RunAsync(statement, parameter);
                    return reader;
                }

            }
            catch (Exception ex)
            {

                throw new NotImplementedException(ex.Message);
            }
            finally
            {
                // Dispose();
            }
        }

        public async Task UpdateTransactionAsync(ITransaction tx, string statement, object parameters)
        {
            try
            {
                await tx.RunAsync(statement, parameters);
            }
            catch (Neo4jException ex)
            {
                throw new Neo4jException(ex.Message);
            }
        }

        public async Task DeleteTransactionAsync(ITransaction tx, string statement, object parameters)
        {
            try
            {
                await tx.RunAsync(statement, parameters);
            }
            catch (Neo4jException ex)
            {
                throw new Neo4jException(ex.Message);
            }
        }

        public async Task CreateTransactionAsync(ITransaction tx, string statement, object parameters)
        {
            try
            {
                await tx.RunAsync(statement, parameters);
            }
            catch (Neo4jException ex)
            {
                throw new Neo4jException(ex.Message);
            }
        }
    }
}
