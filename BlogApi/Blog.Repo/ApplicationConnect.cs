﻿using Microsoft.Extensions.Options;
using Neo4j.Driver.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Repo
{
    public class ApplicationConnect: IApplicationConnect
    {
        public IDriver Driver { get; set; }
        private Neo4jInfor _objNeo4j;
        public ApplicationConnect(IOptions<Neo4jInfor> objNeo4j)
        {
            _objNeo4j = objNeo4j.Value;
            Driver = GraphDatabase.Driver(_objNeo4j.Uri, AuthTokens.Basic(_objNeo4j.User, _objNeo4j.PassWord));
           
        }
        

        IDriver IApplicationConnect.GetDriver()
        {
            return Driver;
        }
        public void Dispose()
        {
            Driver?.Dispose();
        }
    }
}
