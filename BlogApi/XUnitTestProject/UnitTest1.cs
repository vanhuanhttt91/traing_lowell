using System;
using Xunit;
using Blog.Data;
using Blog.Repo;
using Blog.service;
using BlogApi.Controllers;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Blog.Api.Controllers;
using Moq;
using System.Collections.Generic;

namespace XUnitTestProject
{
    public class UnitTest1
    {
        public static string postId = Guid.NewGuid().ToString();
        public readonly string relationCategories = "3c969ec1-ae42-48f9-97eb-9fd5b831633a";

        [Fact]
        public async Task GetPost()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            var objPost = new Post
            {
                Id = postId,
                CategoriesId = "-1",
                Slug = "-11-"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Post>(context);
            var service = new PostService(repos);
            var post = await service.GetDataSync();
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }
        [Fact]
        public async Task GetPostDetail()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            var objPost = new Post
            {
                Id = postId,
                CategoriesId = "-1",
                Slug = "-11-"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Post>(context);
            var service = new PostService(repos);
            var post = await service.GetDetailsSync(postId);
            var result = 0;
            var check = post.Item as List<Post>;
            // Assert
            Assert.Equal(check.Count >1, true);
        }

        [Fact]
        public async Task EditPost()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            var objPost = new Post
            {
                Id = postId,
                CategoriesId = "-1",
                Slug = "-11-"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Post>(context);
            var service = new PostService(repos);
            var post = await service.EditSync(objPost);
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }

        [Fact]
        public async Task DeletePost()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Post>(context);
            var service = new PostService(repos);
            var post = await service.DeleteSync(postId);
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }

        [Fact]
        public async Task EditCategories()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            var objectCate = new Categories
            {
                Id = postId,
                Description = "-1",
               
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Categories>(context);
            var service = new CategoriesService(repos);
            var post = await service.EditSync(objectCate);
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }

        [Fact]
        public async Task DeleteCategories()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Categories>(context);
            var service = new CategoriesService(repos);
            var post = await service.DeleteSync(postId);
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }
        [Fact]
        public async Task GetCategories()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Categories>(context);
            var service = new CategoriesService(repos);
            var post = await service.GetDataAsync();
            var result = false;
            var check = post.IsError;
            // Assert
            Assert.Equal(check, result);
        }

        [Fact]
        public async Task GetCategoriesDetail()
        {
            // Arrange
            var connection = new Neo4jInfor
            {
                PassWord = "123",
                User = "neo4j",
                Uri = "bolt://localhost:7687"
            };
            IOptions<Blog.Repo.Neo4jInfor> options = Options.Create(connection);
            var context = new ApplicationConnect(options);
            var repos = new Repository<Categories>(context);
            var service = new CategoriesService(repos);
            var post = await service.GetDataByPostIdAsync(postId);
            var result = 0;
            var check = post.Item as List<Categories>;
            // Assert
            Assert.Equal(check.Count, result);
        }
    }
}
