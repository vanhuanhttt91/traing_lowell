# Blog



 

# About blog!
  Building a simple Blog Engine using ASP.NET Core

### LInk URL      
  
 -  Link: [http://192.168.194.207:8082](http://192.168.194.207:8082)
 -  Login:  UserName/Password: vanhuan@gmail.com/V@nHu@n91

You can also:
  - For blog owner:
  - -Login/Logout
  - -Manage categories (Name, Description)
  - -Manage post (Slug, ShortDescription, Content, ThumbnailImage, CreatedDate, UpdatedDate)
  - -For internet users:
  - -Home page: lasted posts, categories menu
  - -View posts by category
  - -View post details

 
### Technology

Fontend:

* [vuecli] - single page app (-v3.0)!
* [VueFroala] - text editor
* [Bootstrap vue] - great UI boilerplate for modern web apps
* [vuesax] - design control, component, notify...
* [vee-validate] - validate form
* [Vuex] - two way binding mechanism
* [FontAwesomeIcon] - design icon..
* [jQuery] - wow

BackEnd:

* Asp.net core 2.1 + Api
* Jwt authentication token
* Identity neo4j
* xunittest

DataBase:

* neo4j  graph database
* 


Deploy:

* IIS server
*

### structure Backend:

 Descipton: using Dependency injection.
 
     --- image1: home page
[![N|Solid](https://cdn.pbrd.co/images/HL0jQE6.png)](https://nodesource.com/products/nsolid)

### structure fontend

    Descipton: 
    
     --- image1: Structure
[![N|Solid](https://cdn.pbrd.co/images/HL0ltoq.png)](https://nodesource.com/products/nsolid)


### Layout homepage

    Descipton: sidebar-left show lasted posts (limit 5 posts),header menu show categories menu, login button  and body show post list
     --- image1: home page
[![N|Solid](https://cdn.pbrd.co/images/HKZYShn.png)](https://nodesource.com/products/nsolid)
### View posts by category

    Description: show post by category
[![N|Solid](https://cdn.pbrd.co/images/HL0aino.png)](https://nodesource.com/products/nsolid)

### View post details
    Description: show posts detail include image, text...
[![N|Solid](https://cdn.pbrd.co/images/HL0bB7n.png)](https://nodesource.com/products/nsolid)
### Login
    Description: login for admin, check role 
[![N|Solid](https://cdn.pbrd.co/images/HL0e1A7E.png)](https://nodesource.com/products/nsolid)

### View post manager
    Description: add, delete, edit post
[![N|Solid](https://cdn.pbrd.co/images/HL0ftd3.png)](https://nodesource.com/products/nsolid)
### View categories manager
    Description: login for admin, check role 
[![N|Solid](https://cdn.pbrd.co/images/HL0grz2.png)](https://nodesource.com/products/nsolid)

For production environments...

### Deploy
    IIS server
     


